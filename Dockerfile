FROM ubuntu:18.04

RUN apt-get update && \
    apt-get install -y python3 && \
    apt-get install -y python-pip && \
    apt-get install -y python3-flask

COPY . .
COPY requirements.txt /requirements.txt

RUN pip install -r requirements.txt

EXPOSE 5000
EXPOSE 3306

CMD ["python", "./QSEIndex/QSEIndex/main.py"]
