import json
import os

import scrapy
from flask import Flask, request, jsonify, render_template
from flask_cors import CORS
from flaskext.mysql import MySQL
from QSEIndex.QSEIndex.spiders.qsespider import QSESpider as spiderobj

app = Flask(__name__)
CORS(app)
# Database connection info. Note that this is not a secure connection.
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = ''
app.config['MYSQL_DATABASE_DB'] = 'store'
app.config['MYSQL_DATABASE_HOST'] = '34.77.42.44'
app.config['MYSQL_DATABASE_SOCKET'] = ''
mysql = MySQL()
mysql.init_app(app)
conn = mysql.connect()
cursor = conn.cursor()


# endpoint for search

# @app.route("/")
# def main():
#     return render_template('home.html')


@app.route("/", methods=['GET', 'POST'])
def search():
    if request.method == "POST":
        item = request.form['item']
        sql_select_query = "SELECT uri FROM page WHERE content LIKE '%" + item + "%';"
        sql_select_query2 = "SELECT keyword FROM advert WHERE advert LIKE '%" + item + "%';"
        cursor.execute("USE store;")
        cursor.execute(sql_select_query)
        conn.commit()
        data = cursor.fetchall()

        cursor.execute(sql_select_query2)
        conn.commit()
        data2 = cursor.fetchall()

        if len(data) == 0 and item == 'all':
            cursor.execute("SELECT uri from page")
            conn.commit()
            data = cursor.fetchall()

        return render_template('home.html', data=data, data2=data2)
    return render_template('home.html')


@app.route("/display", methods=['GET', 'POST'])
def display():
    if request.method == "GET":
        selecteditem = request.args.get('selecteditem')
        sql_select_query = "SELECT content FROM page WHERE uri LIKE '%" + selecteditem + "%';"
        sql_select_query2 = "SELECT advert FROM advert WHERE keyword LIKE '%" + selecteditem + "%';"
        cursor.execute("USE store;")
        cursor.execute(sql_select_query)
        conn.commit()
        data = cursor.fetchall()

    return render_template('display.html', data=data)


@app.route("/display2", methods=['GET', 'POST'])
def display2():
    if request.method == "GET":
        selecteditem = request.args.get('selecteditem')
        sql_select_query = "SELECT advert FROM advert WHERE keyword LIKE '%" + selecteditem + "%';"
        cursor.execute("USE store;")
        cursor.execute(sql_select_query)
        conn.commit()
        data = cursor.fetchall()

    return render_template('display.html', data=data)


@app.route("/spider", methods=['GET', 'POST'])
def spider():
    if request.method == "POST":
        x = request.form['item']
        os.system("scrapy crawl qse -a domain="+x+"")
        sql_select_query = "SELECT content FROM page WHERE uri LIKE '%" + x + "%';"
        cursor.execute("USE store;")
        cursor.execute(sql_select_query)
        conn.commit()
        data = cursor.fetchall()

    return render_template('spider.html')


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
