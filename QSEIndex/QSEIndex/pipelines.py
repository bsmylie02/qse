# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import mysql.connector

class QseindexPipeline(object):

    def __init__(self):
        self.curr, self.conn = self.create_connection()
        self.curr = self.create_table(self.curr)

    def create_connection(self):
        conn = mysql.connector.connect(
            host='34.77.42.44',
            database='store',
            user='root',
            password='',
            auth_plugin = 'mysql_native_password'
        )
        return conn.cursor(), conn

    def create_table(self,curr):
        curr.execute("""create table if not exists page(
                    `pageid` bigint(20) UNSIGNED NOT NULL,
                    `uri` varchar(254) NOT NULL,
                    `indexedat` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    `content` text NOT NULL
                    )""")
        #curr.execute("""ALTER TABLE `page`
        #            ADD PRIMARY KEY (`pageid`);
        #            """)
        curr.execute("""ALTER TABLE `page`
                         MODIFY `pageid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
                    """)
        return curr

    def process_item(self, item, spider):
        self.store_db(item)
        return item

    def store_db(self, item):
        self.curr.execute("""insert into page (uri, content) values (%s,%s)""", (
            item['uri'],
            item['content'],
        ))

        self.conn.commit()
