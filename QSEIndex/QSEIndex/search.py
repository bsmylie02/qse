import mysql


def search(x):
    # try:
    connection = mysql.connector.connect(host='localhost',
                                         database='store',
                                         user='root',
                                         password='')

    sql_select_query = "SELECT * FROM store WHERE content LIKE %" + x + "%"
    cursor = connection.cursor()
    cursor.execute(sql_select_query)
    records = cursor.fetchall()
    print("Total number of rows in store is: ", cursor.rowcount)

    print("\nPrinting each store record")
    for row in records:
        print("Id = ", row[0], )
        print("Url = ", row[1])
        print("Timestamp  = ", row[2])

    # except error as e:
    # print("Error reading data from MySQL table", e)

    if connection.is_connected():
        connection.close()
        cursor.close()
        print("MySQL connection is closed")

    return records
