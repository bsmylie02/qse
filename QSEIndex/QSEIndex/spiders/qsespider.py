import scrapy
from scrapy.selector import Selector
from ..items import QseindexItem
import xpath


class QSESpider(scrapy.Spider):
    name = "qse"

    def __init__(self, domain='', *args, **kwargs):
        super(QSESpider, self).__init__(*args, **kwargs)
        self.start_urls = [domain]

    # def start_requests(self, x):
    #     urls = [
    #         # 'http://quotes.toscrape.com/page/1/',
    #         # 'https://en.wikipedia.org/wiki/Computer',
    #         # 'https://en.wikipedia.org/wiki/Arithmetic',
    #         # 'https://en.wikipedia.org/wiki/Boolean_algebra',
    #         # 'https://en.wikipedia.org/wiki/Software',
    #         # 'https://en.wikipedia.org/wiki/Executable',
    #         # 'https://en.wikipedia.org/wiki/Executable',
    #         # 'https://en.wikipedia.org/wiki/Hardware',
    #     ]

      #  yield scrapy.Request(x, callback=self.parse)

    def take_requests(self, x):
        yield scrapy.Request(url=x, callback=self.parse)

    def parse(self, response):
        items = QseindexItem()
        sel = Selector(response)
        newurl = response.request.url
        content = u''.join(sel.xpath('//body/descendant-or-self::*[not(self::script)]/text()').extract()).strip()

        items['uri'] = newurl
        items['content'] = content

        yield items
